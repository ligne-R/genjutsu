#!/usr/bin/env python3
"""Generator for the Ninja build system."""
import dataclasses
import logging.config
import sys
from argparse import ArgumentParser
from collections import OrderedDict, defaultdict, namedtuple
from contextlib import ExitStack, contextmanager, suppress
from dataclasses import dataclass
from functools import lru_cache, partial, reduce
from inspect import stack
from io import TextIOBase
from itertools import chain, groupby, repeat
from operator import itemgetter
from os import cpu_count, environ, pathsep
from os.path import expandvars
from pathlib import Path, PurePath
from sys import getprofile, setprofile
from types import SimpleNamespace
from typing import (AbstractSet, Any, Callable, Collection, Dict, Final,
                    FrozenSet, Iterable, Iterator, List, Mapping, MutableMapping, MutableSet, Optional,
                    Tuple, Union, cast, Literal, Sequence)

assert sys.version_info > (3, 6)

__version__ = '1.0.0+20170613.0'
__author__ = 'ligne-R'

PathLike = Union[PurePath, str]

_RESOURCE_DIR = Path(__file__).parent / '../resources'
_RESOURCE_PATH = tuple(chain(expandvars(environ.get('GENJUTSU_RESOURCE_PATH', '')).split(pathsep), (Path.cwd(), _RESOURCE_DIR)))

_DEFINITIONS: MutableMapping[str, Union[PathLike, int]] = dict(PYTHON=sys.executable, NPROC=cpu_count() or 1)

DEFAULT_FLAVOUR = 'default'

_BUILD_DIR: Optional[Path] = None

_GENERATED_FILE_PREFIX = '_'

def is_path_like(path):
    return isinstance(path, (PurePath, str))

Flavour = Union[Literal[DEFAULT_FLAVOUR], str]

@dataclass(order=True, frozen=True)
class VariableType:
    name: str
    value: str
    append: bool

@dataclass(frozen=True)
class TargetType:
    env: 'Env'
    inputs: Collection[Union[PurePath, 'TargetType', 'FilterType']]
    implicit_inputs: Collection[Union[PurePath, 'TargetType', 'FilterType']]
    order_only_inputs: Collection[Union[PurePath, str]]
    rule: str
    flags: Collection[VariableType]
    outputs: Collection[Union[PurePath, str]]
    implicit_outputs: Collection[Union[PurePath, str]]
    exported_flags: Collection[VariableType]

@dataclass(frozen=True)
class FilterType:
    inputs: Iterable[TargetType]
    outputs: Iterable[Union[PurePath, str]]


""" `Variable` overrides, `Flags` concatenates """
Variable, Flag = partial(VariableType, append=False), partial(VariableType, append=True)  # pylint: disable=invalid-name

TargetOrPath = Union[TargetType, PathLike]
StrDict = Dict[str, Any]
ToolsetFunc = Callable[[], None]
InjectionFunc = Callable[['Env', str], str]

def get_resource_file(filename: PathLike, *, search_base_dir: PathLike = None) -> Path:
    """Search for a file in resource dirs

        args:
            filename: name of the file
            search_base_dir: directories to search into in priority before standard resource dirs
    """
    candidates: Final[Iterable[Path]] = (Path(cast(PathLike, search_dir)) / filename for search_dir in filter(None, (search_base_dir, *_RESOURCE_PATH, *sys.path)))
    with suppress(StopIteration):
        return next(path.resolve() for path in candidates if path.is_file())
    raise FileNotFoundError(filename)


class Env:
    __stack:List['Env'] = []
    __initializers:MutableSet[Callable[[], None]] = set()

    @classmethod
    @contextmanager
    def _pushed(cls, env: 'Env') -> Iterator['Env']:  # pylint:disable=redefined-outer-name
        logging.debug(f'Push {env}')
        was_empty = len(cls.__stack) == 0
        cls.__stack.append(env)
        if was_empty:
            forward = getprofile() or (lambda frame, even, arg: None)
            def profile(frame, event, arg):
                if event == 'call' and not (filename := frame.f_code.co_filename).startswith('<'):
                    cls.__stack[-1].add_dependency(filename)
                forward(frame, event, arg)
            setprofile(profile)
        try:
            yield env
        finally:
            if was_empty:
                setprofile(forward)
            cls.__stack.pop()
            logging.debug('Pop')

    @classmethod
    def _head(cls)->'Env':
        if not cls.__stack:
            raise RuntimeError('No Env context')
        return cls.__stack[-1]

    @classmethod
    def register_initializer(cls, initializer:Callable[[], None]):
        """Register initializer, to be called on the current and future Env"""
        logging.debug('Register initializer %s', initializer)
        initializer()
        cls.__initializers.add(initializer)


    def __init__(self, prj_file: PathLike, *, source_dir: PathLike='.', build_dir: PathLike=f'{_GENERATED_FILE_PREFIX}build', ninja_file:PathLike='build.ninja', supenv:Optional['Env']=None):
        """`Env` constructor.

            args:
                prj_file: project file
                source_dir: base directory for sources
                build_dir: build output dir
                ninja_file: generated ninja file
                supenv: parent environment
        """
        super().__init__()
        self.__prj_file = Path(prj_file).resolve()
        self.__ninja_file = ninja_file
        self.__source_dir = Path(source_dir)
        self.__build_dir = PurePath(build_dir)
        self.__supenv: Final[Optional['Env']] = supenv

        self.__flags_by_flavour: Final[Mapping[Flavour, List[VariableType]]] = defaultdict(list, {DEFAULT_FLAVOUR: []})
        self.__targets : Final[Dict[PathLike, TargetType]] = {}
        self.__subenvs: Tuple['Env', ...] = ()
        self.__defaults : Tuple[TargetType, ...] = ()
        self.__injections : Final[Dict[Any, InjectionFunc]] = {}

        self.__lineno: Final[int] = next((lineno for _, frame_filename, lineno, *_ in stack(context=0) if Path(frame_filename).exists() and self.__prj_file is not None and self.__prj_file.samefile(frame_filename)), 0)
        self.__dependencies: Final[MutableSet[PathLike]] = set()

        with self._pushed(self):
            for initializer in self.__initializers:
                initializer()

    def __str__(self):
        return f'{self.prj_file!s}:{self.__lineno}'

    @property
    def prj_file(self) -> Path:
        """Absolute path to the environment project path."""
        return self.__prj_file

    @property
    def base_dir(self) -> Path:
        """Absolute path to the directory of the environment project path."""
        return self.prj_file.parent

    def get_source_path(self, *, absolute=True) -> Path:
        """Path to the directory of the environment source directory."""
        return self.base_dir / self.__source_dir if absolute else self.__source_dir

    """Path to the directory of the environment source directory, relative to the base directory."""  # pylint: disable=pointless-string-statement
    source_dir = source_path = cast(Path, property(partial(get_source_path, absolute=False)))

    @property
    def build_dir(self) -> PurePath:
        """Path to the directory of the environment build directory, relative to the base directory."""
        return self.__build_dir

    def get_build_path(self, *, flavour:Flavour=DEFAULT_FLAVOUR, absolute=True) -> PurePath:
        return (self.base_dir / self.build_dir if absolute else self.build_dir) / (flavour if flavour != DEFAULT_FLAVOUR else '{flavour}')

    """Relative to base_dir"""  # pylint: disable=pointless-string-statement
    build_path = property(partial(get_build_path, flavour=DEFAULT_FLAVOUR, absolute=False))

    @property
    def ninja_file(self) -> Optional[Path]:
        """Absolute"""
        return self.base_dir / self.__ninja_file if self.__ninja_file else None

    @property
    def dependencies(self) -> Iterable[Path]:
        """Recursively lists the environment dependencies"""
        return frozenset(Path(dependency).resolve() for dependency in chain(self.__dependencies, *(env.dependencies for env in self.all_subenvs)))

    def add_dependency(self, dependency: Path):
        """Adds a dependency (path).
           Dependencies are files from which the current environment is generated, and has to be updated if one of them is altered
        """
        self.__dependencies.add(dependency)

    @property
    def supenv(self):
        """Returns the parent environment (or None)"""
        return self.__supenv

    @property
    def first_class_supenv(self):
        """Returns the first parent environment in the parent hierarchy which is the toplevel environment in its project definition file."""
        return self if self.ninja_file or not self.supenv else self.supenv.first_class_supenv

    @property
    def all_subenvs(self) -> AbstractSet['Env']:
        """Recursively returns every children environments."""
        return frozenset(self.__subenvs).union(*(env.all_subenvs for env in self.__subenvs))

    @property
    def local_subenvs(self) -> AbstractSet['Env']:
        """Recursively returns every children environments belonging to the same project definition file."""
        local_subenvs = {env for env in self.__subenvs if not env.ninja_file}
        return frozenset(local_subenvs).union(*(env.local_subenvs for env in local_subenvs))

    @property
    def first_class_subenvs(self) -> AbstractSet['Env']:
        """Recursively returns every children environments which are the toplevel environments in their project definition file."""
        return frozenset(env for env in self.all_subenvs if env.ninja_file)

    def add_subenv(self, env: 'Env'):  # pylint:disable=redefined-outer-name
        """Adds a child environment."""
        self.__subenvs += (env,)
        return env

    @property
    def flags(self) -> Iterable[VariableType]:
        return self.__flags_by_flavour[DEFAULT_FLAVOUR]

    @property
    def parent_flags(self) -> Iterable[VariableType]:
        return (*(self.supenv.parent_flags if self.supenv else ()), *self.flags)

    @property
    def local_flags(self) -> Iterable[VariableType]:
        return (*(self.supenv.local_flags if self.supenv else ()), *(self.flags if not self.ninja_file else ()))

    def add_flag(self, flag: VariableType, flavour: Flavour=DEFAULT_FLAVOUR) -> VariableType:
        self.__flags_by_flavour[flavour].append(flag)
        return flag

    @property
    def flags_by_flavour(self) -> Mapping[Flavour, Tuple[VariableType, ...]]:
        """Returns a dictionaty of flavour-specific flags by flavour name for the environment and its parents"""
        parent_flags: Final[Mapping[Flavour, List[VariableType]]] = self.supenv.flags_by_flavour if self.supenv and not self.ninja_file else {}
        return {name: parent_flags.get(name, ()) + tuple(self.__flags_by_flavour.get(name, ())) for name in parent_flags.keys() ^ self.__flags_by_flavour.keys()}

    @property
    def all_flavours(self) -> AbstractSet[Flavour]:
        return self.flags_by_flavour.keys()

    @property
    def targets(self) -> AbstractSet[TargetType]:
        return frozenset(self.__targets.values())

    @lru_cache(maxsize=None)
    def __make_target(self, *args):
        target = TargetType(self, *args)
        logging.debug(f'Create target {",".join(map(str, target.outputs))}')
        if self.__targets.keys() & target.outputs:
            raise RuntimeError(f'{",".join(map(str, self.__targets.keys() & target.outputs))} already have a target')
        self.__targets.update((output, target) for output in target.outputs)
        return target

    def make_target(self, inputs: Iterable[Union[PurePath, TargetType, FilterType]], outputs: Iterable[Union[PurePath, str]], rule: str,
                    implicit_inputs: Iterable[Union[PurePath, TargetType, FilterType]]=(), order_only_inputs: Iterable[Union[PurePath, str]]=(),
                    implicit_outputs: Iterable[Union[PurePath, str]]=(), flags: Iterable[VariableType]=(), exported_flags: Iterable[VariableType]=()):
        return self.__make_target(tuple(inputs), tuple(implicit_inputs), tuple(order_only_inputs), rule, tuple(flags), tuple(outputs), tuple(implicit_outputs),
                                  tuple(exported_flags))

    def target_of(self, output):
        return self.__targets.get(output)

    @property
    def all_targets(self) -> AbstractSet[TargetType]:
        """Recursively return all the targets"""
        return cast(FrozenSet[TargetType], self.targets).union(*(env.targets for env in self.all_subenvs))

    @property
    def local_targets(self) -> AbstractSet[TargetType]:
        """Recursively return all the targets from the children environments belonging to the same project definition file."""
        return cast(FrozenSet[TargetType], self.targets).union(*(env.targets for env in self.local_subenvs))

    @property
    def terminal_targets(self) -> AbstractSet[TargetType]:
        """Recursively return all the targets which are leaves of the target dependency tree"""
        return cast(FrozenSet[TargetType], self.local_targets).difference(*(chain(target.inputs, target.implicit_inputs) for target in self.local_targets))


    @property
    def all_defaults(self)->Iterable[TargetType]:
        """Recursively lists the default targets of the environment and its children"""
        return frozenset(chain(self.__defaults, *(env.all_defaults for env in self.all_subenvs)))

    def add_default(self, default:TargetType)->None:
        """Register a default target"""
        self.__defaults += (default,)

    @property
    def all_actual_defaults(self)->Iterable[TargetType]:
        return self.all_defaults or tuple(chain.from_iterable(subenv.terminal_targets for subenv in (self, *self.first_class_subenvs)))

    @property
    def all_injections(self) -> Iterable[InjectionFunc]:
        """Recursively lists the default injections of the environment and its children"""
        def injections_recursive(env):  # pylint:disable=redefined-outer-name
            return chain(env.__injections.items(), *(injections_recursive(subenv) for subenv in env.all_subenvs))  # pylint:disable=protected-access
        return tuple(dict(injections_recursive(self)).values())

    def add_injection(self, injection: InjectionFunc, *, key:Any=None) -> None:
        """Registers an injection"""
        self.__injections[key if key is not None else injection] = injection


class _EnvHead:
    @property
    def actual(self):
        return Env._head()  # pylint:disable=protected-access

    def __getattr__(self, name):
        return getattr(Env._head(), name)


"""Read-only proxy to the current (ie. top of stack) :class:`Env` instance."""  # pylint: disable=pointless-string-statement
E = _EnvHead()


@contextmanager
def env(path=Path(), **kwargs) -> Iterator['Env']:
    """Convenient function to create a new sub environment to the current :class:`Env`.

        args:
            path: relative path of the new environment
            kwargs: forwarded to the constructor of :class:`Env`
        yields:
            Env: new environment
    """
    default_kwargs = dict(prj_file=E.prj_file, source_dir=E.source_dir / path, build_dir=E.build_dir / path, ninja_file=None, supenv=E.actual)
    subenv = Env(**dict(chain(default_kwargs.items(), kwargs.items())))  # pylint:disable=redefined-outer-name
    with Env._pushed(subenv):  # pylint:disable=protected-access
        yield subenv
    E.add_subenv(subenv)


def Target(inputs: Iterable[Union[PurePath, TargetType, FilterType]], outputs: Iterable[Union[PurePath, str]], rule: str,  # pylint:disable=invalid-name
           *, implicit_inputs: Iterable[Union[PurePath, TargetType, FilterType]]=(), order_only_inputs: Iterable[Union[PurePath, str]]=(),
           implicit_outputs: Iterable[Union[PurePath, str]]=(), flags: Iterable[VariableType]=(), exported_flags: Iterable[VariableType]=()) -> TargetType:
    """Base type of every node in the graph of dependencies.
        Outputs a `build` statement in the Ninja file

        args:
            inputs :
            outputs :
            rule :
            implicit_inputs :
            order_only_inputs :
            implicit_outputs :
            flags :
            exported_flags :
        returns:
            Target
    """
    return E.make_target(tuple(inputs), tuple(outputs), rule, tuple(implicit_inputs), tuple(order_only_inputs), tuple(implicit_outputs), tuple(flags), tuple(exported_flags))


def Alias(name: str, targets: Iterable[TargetType]) -> TargetType:  # pylint: disable=invalid-name
    """"Phony" target.

        args:
            name : alias name
            targets : target referenced under the alias
        returns:
            Target
    """
    return Target(targets, (name,), 'phony')


def Default(target: TargetType) -> None:  # pylint: disable=invalid-name
    """Adds `target` to the default targets of the build (`default` statement in the Ninja file)."""
    E.add_default(target)


def Inject(injection: Callable[[Env, Flavour], None], *, key: Any=None) -> None:  # pylint: disable=invalid-name
    """Injections are extensibility mechanism.

        args:
            injection : function taking an Env and a flavour arg as parameters, called during generation
            key : uniquely defines an injection. A new :class:`Injection` override an existing one sharing the same key
    """
    E.add_injection(injection, key=key)


def Apply(*args: Iterable[Union[VariableType, Iterable[VariableType]]], flavour: Flavour=DEFAULT_FLAVOUR) -> Collection[VariableType]:  # pylint: disable=invalid-name
    """Applies a set of :class:`Flag` to the current environment.

        args:
            args : single or sequence of :class:`Flag`
            flavour :
    """
    return tuple(chain.from_iterable((E.add_flag(flag, flavour=flavour),) if not isinstance(flag, Iterable) or isinstance(flag, VariableType) else chain.from_iterable(map(partial(Apply, flavour=flavour), flag)) for flag in args))


@lru_cache(maxsize=None)
def _Prjdef(path: PathLike, kwargs_: Mapping[str, Any]) -> SimpleNamespace:  # pylint: disable=invalid-name
    logging.debug(f'Parse file {path!s}')
    with Env._pushed(Env(prj_file=path, **dict(kwargs_))) as env_:  # pylint:disable=protected-access
        globals_ = {k: v for k, v in vars(sys.modules[__name__]).items() if not k.startswith('_')}
        globals_['__file__'] = globals_['__prjdef__'] = E.prj_file
        with E.prj_file.open() as stream:
            initial_keys = frozenset(globals_.keys())
            exec(compile(stream.read(), stream.name, 'exec', optimize=0), globals_, globals_)  # pylint: disable=exec-used
            globals_ = dict(((k, v) for k, v in globals_.items() if k not in initial_keys and not k.startswith('_')), E=env_)
            return SimpleNamespace(**globals_)


def Prjdef(path: PathLike, **kwargs: Mapping[str, Any]) -> SimpleNamespace:  # pylint: disable=invalid-name
    """Include other projects.

        args:
            path: path to a prjdef file or a directory containing a `prjfdef` file (absolute, or relative to the current Environment dir)
            kwargs: forwarded to the constructor of Env

        returns:
            namedtuple: namespace of the project
    """
    env: Optional[Env] = None  # pylint:disable=redefined-outer-name
    with suppress(RuntimeError):
        env = E.actual
    resolved_path: Path = cast(Path, (env.base_dir if env else Path(sys.modules['__main__'].__file__).parent) / path).resolve()
    resolved_path = resolved_path / ('prjdef' if resolved_path.is_dir() else '')
    prjdef: SimpleNamespace = _Prjdef(resolved_path, frozenset(kwargs.items()))
    if env:
        env.add_subenv(prjdef.E)
    return prjdef


def Glob(pattern: str, *, root:Optional[Path]=None) -> Iterable[PurePath]:  # pylint: disable=invalid-name
    """Just like glob module.

        args:
            pattern: glob-style pattern
            root: search dir root, defaults to :class:`E``.source_path`

        yields:
            Path: matching file
    """
    for k in (root or E.get_source_path()).glob(pattern):
        yield k.relative_to(E.get_source_path())


def Filter(inputs: Iterable[TargetType], pattern_or_function: Union[str, Callable[[PathLike], bool]]):  # pylint: disable=invalid-name
    """Filters among the outputs of declared :class:`Target`.

        example:
        `Filter(input_targets, file_match('*.o'))`

        args:
            inputs: Targets which `outputs`
            pattern_or_function: if a function, only keep `outputs` for which `function(output)` is `True`
                                 else glob-style pattern
    """
    predicate = pattern_or_function if callable(pattern_or_function) else lambda output: isinstance(output, PurePath) and output.match(pattern_or_function)
    return FilterType(inputs, tuple(chain.from_iterable(filter(predicate, input.outputs) for input in inputs)))


def resolve(value: Union[str, PurePath, TargetType, VariableType, Iterable[Any]], *, env: Optional[Env]=None, flavour:Optional[Flavour]=None, quote_paths:bool=False):  # pylint: disable=redefined-outer-name
    """Recursively traverse `value`"""
    recurse = partial(resolve, env=getattr(value, 'env', env), flavour=flavour, quote_paths=quote_paths)

    if isinstance(value, str):
        yield value if flavour is None else value.format(flavour=flavour)
    elif isinstance(value, PurePath):
        result = PurePath(next(recurse(str(env.base_dir / value if env else value))))
        yield f'"{result}"' if quote_paths else result
    elif isinstance(value, TargetType):
        yield from recurse(value.outputs)
    elif isinstance(value, VariableType):
        yield value.replace(value=resolve(value.value))
    elif isinstance(value, Iterable):
        for item in value:
            yield from recurse(item)
    else:
        yield value


def resolve_as_input(input_: TargetOrPath, *, env: Optional[Env]=None) -> PurePath:
    """Helper function to be used in toolsets"""
    return PurePath(input_.outputs[0]) if hasattr(input_, 'outputs') else (env or E).source_path / input_


def escape_path(path: PathLike):
    """Escapes path for use in ninja."""
    return str(path).replace('$ ', '$$ ').replace(' ', '$ ').replace(':', '$:')


def resolve_escape_join_path(path: PathLike, *, env: Env, flavour: Optional[Flavour]):  # pylint: disable=redefined-outer-name
    return ' '.join(escape_path(item) for item in resolve(path, env=env, flavour=flavour))


def _resolve_reduce_flag(flag: VariableType, *, env: Env, flavour: Optional[Flavour]):  # pylint: disable=redefined-outer-name
    return ''.join(str(item) for item in resolve(flag, env=env, flavour=flavour, quote_paths=True))


def _merge_flags(flag0: VariableType, flag: VariableType):
    return dataclasses.replace(flag, value=(flag0.value, flag.value)) if flag0.value and flag.append else flag


def _merge_flags_iterable(flags: Iterable[VariableType]):
    return sorted(reduce(lambda flags_dict, flag: dict(chain(flags_dict.items(), ((flag.name, _merge_flags(flags_dict.get(flag.name, Variable(flag.name, ())), flag)),))), flags, {}).values())


def _resolve_flags(flags: Iterable[VariableType], *, env: Env, flavour: Flavour):  # pylint: disable=redefined-outer-name
    return (dataclasses.replace(flag, value=_resolve_reduce_flag(flag.value, env=env, flavour=flavour)) for flag in flags)


def get_target_flags(target: TargetType, flavour: Flavour):
    resolve_flags_ = partial(_resolve_flags, flavour=flavour)

    def inputs_flags(target):
        return chain(resolve_flags_(target.exported_flags, env=target.env), chain.from_iterable(inputs_flags(input_)
               for input_ in chain(target.inputs, target.implicit_inputs) if isinstance(input_, TargetType)))
    return _merge_flags_iterable(chain(resolve_flags_(target.env.local_flags, env=target.env), resolve_flags_(target.flags, env=target.env),
                                       chain.from_iterable(inputs_flags(input_) for input_ in chain(target.inputs, target.implicit_inputs)
                                                                                if isinstance(input_, TargetType))))


def get_env_flags(env: Env, flavour: Flavour):  # pylint: disable=redefined-outer-name
    return _merge_flags_iterable(chain.from_iterable(_resolve_flags(flags, env=env, flavour=flavour) for flags in (env.parent_flags, env.flags_by_flavour.get(flavour, ()))))


def _generate_env(env: Env):  # pylint:disable=redefined-outer-name

    def phony_inputs(targets: Iterable[TargetType]):
        return reduce(frozenset.union, (phony_inputs(target.inputs) if target.rule == 'phony' else frozenset((target,)) for target in targets), frozenset())

    with ExitStack() as stack:  # pylint:disable=redefined-outer-name
        def file_(filename: Path, flavour:Flavour=DEFAULT_FLAVOUR):
            @contextmanager
            def context_(filename) -> Iterator[TextIOBase]:
                try:
                    logging.debug(f'generate {filename!s}')
                    filename.parent.mkdir(parents=True, exist_ok=True)
                    with filename.open('w') as out:
                        yield out
                except:
                    filename.unlink()
                    raise
            return stack.enter_context(context_(Path(next(resolve(filename, env=env, flavour=flavour)))))

        main_build_file = file_(env.base_dir / env.ninja_file.name)
        deps_file = file_(env.base_dir / f'{_GENERATED_FILE_PREFIX}{env.ninja_file.name}.d')
        common_build_file = file_(env.base_dir / (f'{_GENERATED_FILE_PREFIX}common_{env.ninja_file.name}'))
        build_files = {flavour: file_(dir_ / env.ninja_file.name, flavour) for flavour, dir_ in ((flavour, env.get_build_path(flavour=flavour)) for flavour in env.all_flavours)}
        local_files = {flavour: file_(dir_ / (f'{_GENERATED_FILE_PREFIX}local_{env.ninja_file.name}'), flavour) for flavour, dir_ in ((flavour, env.get_build_path(flavour=flavour)) for flavour in env.all_flavours)}

        main_build_file.write('ninja_required_version=1.10\n')
        if _BUILD_DIR is not None:
            main_build_file.write(f'builddir={escape_path(_BUILD_DIR)}\n')
        main_build_file.write(f'include {escape_path(get_resource_file("common.ninja_inc"))}\n')
        for key, value in _DEFINITIONS.items():
            main_build_file.write(f'{key}={escape_path(value) if is_path_like(value) else str(value)}\n')
        main_build_file.writelines(f'{line}\n' for line in OrderedDict(zip(chain.from_iterable(injection(env, flavour).splitlines() for injection in env.all_injections for flavour in env.all_flavours), repeat(None))).keys())
        main_build_file.writelines(f'subninja {escape_path(subenv.base_dir / f"{_GENERATED_FILE_PREFIX}common_{subenv.ninja_file.name}")}\n' for subenv in (env, *env.first_class_subenvs))

        for flavour in env.flags_by_flavour.keys():
            resolve_escape_join_path_ = partial(resolve_escape_join_path, env=env, flavour=flavour)

            build_files[flavour].write(f'include {escape_path(get_resource_file("common.ninja_inc"))}\n')
            for key, value in _DEFINITIONS.items():
                build_files[flavour].write(f'{key}={escape_path(value)}\n')
            build_files[flavour].writelines(f'{line}\n' for line in OrderedDict(zip(chain.from_iterable(injection(env, flavour).splitlines() for injection in env.all_injections), repeat(None))).keys())
            build_files[flavour].writelines(f'subninja {resolve_escape_join_path_(subenv.base_dir / f"{_GENERATED_FILE_PREFIX}common_{subenv.ninja_file.name}")}\n' for subenv in (env, *env.first_class_subenvs))

            local_files[flavour].writelines(f'{flag.name}={resolve_escape_join_path_((f"${flag.name}", flag.value) if flag.append else flag.value)}\n' for flag in get_env_flags(env, flavour))

            for out_file in (main_build_file, build_files[flavour]):
                out_file.writelines(f'subninja {resolve_escape_join_path_(subenv.get_build_path() / f"{_GENERATED_FILE_PREFIX}local_{subenv.ninja_file.name}")}\n' for subenv in (env, *env.first_class_subenvs))

            for output, items in groupby(sorted(((resolve_escape_join_path_(target.outputs[0]), target) for target in env.all_targets if target.rule == 'phony'), key=itemgetter(0)), key=itemgetter(0)):
                targets = tuple(phony_inputs(target for _, target in items))
                build_files[flavour].write(f'build {output} : phony {resolve_escape_join_path_(targets)}\n')
                main_build_file.write(f'build {output}$:{flavour} : phony {resolve_escape_join_path_(targets)}\n')
                if flavour == 'default':
                    main_build_file.write(f'build {output} : phony {resolve_escape_join_path_(targets)}\n')

        for target in filter(lambda target: target.rule != 'phony', env.local_targets):
            flavour_dependant = all('{flavour}' in str(output) for output in chain(target.outputs, target.implicit_outputs))
            for flavour in env.all_flavours if flavour_dependant else ('default',):
                resolve_escape_join_path_ = partial(resolve_escape_join_path, env=env, flavour=flavour)
                out_file = local_files[flavour] if flavour_dependant else common_build_file
                out_file.write(f'build {resolve_escape_join_path_(target.outputs)} {("| " + resolve_escape_join_path_(target.implicit_outputs)) if target.implicit_outputs else ""} : {target.rule} {resolve_escape_join_path_(target.inputs)} {("| " + resolve_escape_join_path_(target.implicit_inputs)) if target.implicit_inputs else ""}  {("|| " + resolve_escape_join_path_(target.order_only_inputs)) if target.order_only_inputs else ""}\n')
                out_file.writelines(f'  {flag.name}={resolve_escape_join_path_((f"${flag.name}", flag.value) if flag.append else flag.value)}\n' for flag in get_target_flags(target, flavour))

        defaults = phony_inputs(env.all_actual_defaults)
        for flavour in env.all_flavours:
            resolve_escape_join_path_ = partial(resolve_escape_join_path, env=env, flavour=flavour)
            build_files[flavour].write(f'default {resolve_escape_join_path_(defaults)}\n')
            main_build_file.write(f'build {flavour} : phony {resolve_escape_join_path_(defaults)}\n')
        main_build_file.write('default default\n')

        main_build_file.write(f'build {" ".join(escape_path(file_.name) for file_ in chain((main_build_file, deps_file, common_build_file), build_files.values(), local_files.values()))} : genjutsu {escape_path(env.prj_file)}\n')
        deps_file.write(str(env.ninja_file) + ' : ' + ' '.join(str(dep).replace(' ', r'\ ') for dep in sorted(env.dependencies)))


def generate(env):  # pylint:disable=redefined-outer-name
    """Generate ninja build files for env and its children."""
    for subenv in (env, *env.first_class_subenvs):
        _generate_env(subenv)


def assignment(assignment: str):  # pylint:disable=redefined-outer-name
    try:
        return assignment.split('=', 1)
    except ValueError:
        return assignment, None


def main(argv: Optional[Sequence[str]]=None) -> None:
    """CLI entry point."""
    if not argv:
        argv = sys.argv[1:]
    argv += environ.get('GENJUTSU_OPTS', '').split()

    parser = ArgumentParser()
    parser.add_argument('input', type=Path, default=Path.cwd(), help='prjdef file (or directory containing one)')
    parser.add_argument('--logging-ini')
    parser.add_argument('--builddir', type=Path, default=None, help='ninja builddir variable')
    parser.add_argument('-D', type=assignment, default=[], nargs='*', help='definition')
    args = parser.parse_args(argv)

    _DEFINITIONS.update(dict(args.D))

    global _BUILD_DIR  # pylint: disable=global-statement
    _BUILD_DIR = args.builddir

    if args.logging_ini:
        logging.config.fileConfig(args.logging_ini)
    else:
        logging.basicConfig(level=logging.INFO)

    env = Prjdef(args.input.resolve()).E  # pylint:disable=redefined-outer-name

    for name, attribute in vars(Env).items():
        if isinstance(attribute, property):
            setattr(Env, name, property(lru_cache(maxsize=1)(attribute.fget)))

    generate(env)


if __name__ == '__main__':
    sys.modules['genjutsu'] = sys.modules['__main__']
    main()
