from pathlib import Path
from typing import Any

from tests.fixtures.profiler import profiler
from tests.fixtures.resources import resources_dir, run_resources_dir


def pytest_addoption(parser: Any) -> None:
    parser.addoption('--profile', type=Path)
