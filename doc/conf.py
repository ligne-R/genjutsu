#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from datetime import datetime
from pathlib import Path
import sys

import sphinx_bootstrap_theme

sys.path.extend((Path(__file__).parent / path).resolve() for path in ('..', '../genjutsu', '../msbuild_gen'))

from genjutsu import __version__ as genjutsu_version, __author__ as genjutsu_author, __name__ as genjutsu_name


# -- General configuration ------------------------------------------------

extensions = ['sphinx.ext.autodoc',
              'sphinx.ext.doctest',
              'sphinx.ext.todo',
              'sphinx.ext.coverage',
              'sphinx.ext.githubpages',
              'sphinx.ext.napoleon',
              'breathe',
              'm2r']

# Add any paths that contain templates here, relative to this directory.
templates_path = ['templates']

source_suffix = ['.md']

master_doc = 'index'

project = f'幻術 / {genjutsu_name}'
version = genjutsu_version
release = genjutsu_version
author = genjutsu_author
copyright = f'{datetime.now().year}, {genjutsu_author}'

language = None

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This patterns also effect to html_static_path and html_extra_path
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']

# The name of the Pygments (syntax highlighting) style to use.
pygments_style = 'tango'

todo_include_todos = True


# -- Options for HTML output ----------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'bootstrap'
html_theme_path = sphinx_bootstrap_theme.get_html_theme_path()

# (Optional) Logo. Should be small enough to fit the navbar (ideally 24x24).
# Path should be relative to the ``_static`` files directory.
html_logo = 'my_logo.png'

# Theme options are theme-specific and customize the look and feel of a
# theme further.
html_theme_options = {
    'navbar_links': [
        ('API', 'genjutsu'),
        ('Github', 'http://github.com/ligne-r/genjutsu', True),
    ],
    'navbar_sidebarrel': False,
    'navbar_pagenav': False,
    # For black navbar, do 'navbar navbar-inverse'
    'navbar_class': 'navbar navbar-inverse',
    'bootswatch_theme': 'slate',
}

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named 'default.css' will overwrite the builtin 'default.css'.
html_static_path = ['static_html']

# -- Options for HTMLHelp output ------------------------------------------

# Output file base name for HTML help builder.
htmlhelp_basename = 'genjutsudoc'

# -- Options for manual page output ---------------------------------------

# One entry per manual page. List of tuples
# (source start file, name, description, authors, manual section).
man_pages = [
    (master_doc, project, 'genjutsu Documentation',
     [author], 1)
]

breathe_projects = {'genjutsu': 'xml'}
breathe_projects_source = {
    'genjutsu': ('../genjutsu', ['genjutsu.py'])
}
breathe_default_project = 'genjutsu'

napoleon_use_admonition_for_examples = True
