# genjutsu API

---

#3

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

#1

.. automodule:: genjutsu.genjutsu
   :members:

#2

.. autodata:: E
   :annotation: Env proxy

#4

.. autodoxygenindex::
   :project: genjutsu
   :outline:
   :no-link:

.. doxygenclass:: Env
   :project: genjutsu
   :members:
