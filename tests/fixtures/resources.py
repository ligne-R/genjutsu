from pathlib import Path
from shutil import copytree
from typing import Final, Any

from pytest import fixture


@fixture
def resources_dir(request: Any) -> Path:
    return Path(request.module.__file__).parent.relative_to(request.config.rootpath).with_suffix('') / 'resources'


@fixture
def run_resources_dir(resources_dir: Path, tmp_path: Path) -> Path:
    result: Final[Path] = tmp_path / resources_dir.stem
    if resources_dir.is_dir():
        copytree(resources_dir, result)
    else:
        result.mkdir()
    return result
