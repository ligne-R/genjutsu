from pathlib import Path
import sys
from typing import Any, Final, Generator, Iterable, Optional, ContextManager
from contextlib import nullcontext

import pprofile
import pytest


@pytest.fixture
def profiler(request: Any) -> Generator[ContextManager[None], None, None]:
    if sys.gettrace():
        yield nullcontext()
        return
    profile: Final[pprofile.Profile] = pprofile.Profile()
    yield profile
    filename: Final[Optional[Path]] = request.config.getoption('--profile')
    if filename is not None:
        with filename.open('w', encoding='utf-8') as out:
            profile.annotate(out)
        with filename.with_suffix('.callgrind').open('w') as out:
            profile.callgrind(out)
