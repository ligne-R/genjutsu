from pathlib import Path
from typing import Final, ContextManager, Iterable
from filecmp import cmpfiles
from os import walk

from testfixtures import compare

from genjutsu import main as genjutsu_main

ROOT_DIR = Path(__file__).parent.resolve()
_RESOURCE_DIR = ROOT_DIR / 'resources'

def check_expected(actual: Path, expected: Path) -> None:
    _, mismatchs, errors = cmpfiles(expected, actual, ((Path(root) / file_).relative_to(expected) for root, _, files in walk(expected) for file_ in files), shallow=False)
    assert(len(errors) == 0)
    for mismatch in mismatchs:
        assert((actual / mismatch).read_text() == (expected / mismatch).read_text())


def test_full(profiler: ContextManager[None], run_resources_dir: Path, resources_dir: Path) -> None:
    with profiler:
        for case in (run_resources_dir / 'cases').glob('*'):
            assert(case.is_dir())
            genjutsu_main(argv=[str(case), '--logging-ini', str(_RESOURCE_DIR / 'logging.ini')])
            check_expected(case, resources_dir / (case.relative_to(run_resources_dir)))
