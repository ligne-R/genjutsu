'''
MS CL genjutsu toolset
'''
from collections import namedtuple
from itertools import chain
from pathlib import Path

from genjutsu import Alias, Apply, E, Flag, Inject, Target, Variable, escape_path

_CxxPair = namedtuple('_CxxPair', ('non_pic', 'pic'))

_RESOURCE_DIR = Path(__file__).resolve().parent


class CompilerToolset:
    _NAME = "mscl"

    @classmethod
    def apply_to_env(cls):  # pylint: disable=missing-docstring
        Inject(lambda env, flavour: f'include {escape_path(_RESOURCE_DIR / f"{cls._NAME}.ninja_inc")}', key=cls)
        Apply(Flag('CLDEFINEFLAGS', f'/D"{define}"') for define in ('_WIN32', '_DEBUG', 'NOMINMAX', 'CRIPPLED'))

    @staticmethod
    def add_rules(globals_):  # pylint: disable=missing-docstring
        globals_['CxxDef'] = lambda key, value=None: Flag('CLDEFINEFLAGS', ('/D"', key, '"="', value, '"') if value else ('/D"', key, '"'))
        globals_['IncludeDir'] = lambda path, system=False: Flag('CLINCLUDEFLAGS', ('/I', Path(path)))
        globals_['CFlag'] = lambda *args, **kwargs: Flag('CFLAGS', *args, **kwargs)

        def pdb(env):
            return env.first_class_supenv.build_dir / (env.first_class_supenv.base_dir.name + '.pdb')

        def Pch(pch, *, name=None, flags=(), order_only_inputs=(), implicit_outputs=(), **kwargs):  # pylint:disable=invalid-name,missing-docstring
            pch = pch.env.base_dir / pch.outputs[0] if hasattr(pch, 'outputs') else Path(pch)
            cxx = Target((E.source_path / pch,), ((E.build_path / pch).with_name(name or pch.name).with_suffix('.cxx'),), 'make_pch_cxx')
            compiled_pch = (E.build_path / pch).with_name(name or pch.name).with_suffix('.pch')
            flags = chain(flags, (Variable('PCH', E.source_path / pch), Variable('PCH_SOURCE', cxx),
                                  Variable('COMPILED_PCH', compiled_pch),
                                  Variable('PDB', pdb(E))))
#            target = Target((), (cxx.outputs[0].with_suffix('.obj'),), 'pch', flags=flags, order_only_inputs=chain((cxx,), order_only_inputs), implicit_outputs=chain(implicit_outputs, (compiled_pch,)), **kwargs)
            target = Target((), (cxx.outputs[0].with_suffix('.obj'),), 'pch', flags=flags, order_only_inputs=chain((cxx,), order_only_inputs), implicit_outputs=implicit_outputs, **kwargs)
            Alias('pch', (target,))
            return target
        globals_['Pch'] = Pch

        def Cxx(cxx, *, name=None, pch=None, pic=False, implicit_inputs=(), flags=(), exported_flags=(), quick_build_alias='{source}:{flavour}', **kwargs):  # pylint:disable=invalid-name,missing-docstring
            if pch:
                pch_cxx, pch_obj = pch.order_only_inputs[0], pch.outputs[0]
                flags = chain(flags, (Flag('CLFLAGS', ('/Yu', pch_cxx)), Flag('CLFLAGS', ('/FI', pch_cxx))))
                exported_flags = chain(exported_flags, (Flag('PCH_OBJ', pch_obj),))
                implicit_inputs = chain(implicit_inputs, (pch_cxx, pch_obj))
            flags = chain(flags, (Variable('PDB', pdb(E)),))
            cxx_resolved = cxx.outputs[0] if hasattr(cxx, 'outputs') else E.source_path / cxx
            output = (E.build_path / cxx_resolved).with_name(name or cxx_resolved.name).with_suffix('.obj')
            result = Target((cxx_resolved,), (output,), 'cxx', implicit_inputs=implicit_inputs, flags=flags, exported_flags=exported_flags, **kwargs)
            if quick_build_alias is not None:
                Alias(quick_build_alias.format(source=E.base_dir / cxx_resolved, flavour='{flavour}'), (result,))
            return result
        globals_['Cxx'] = Cxx

        def CxxPair(*args, **kwargs):
            result = Cxx(*args, **kwargs)
            return _CxxPair(result, result)
        globals_['CxxPair'] = CxxPair


class ClangCompilerToolset(CompilerToolset):
    _NAME = "mscl-clang"


class LinkerToolset:
    @classmethod
    def apply_to_env(cls):  # pylint: disable=missing-docstring
        Inject(lambda env, flavour: f'include {escape_path(_RESOURCE_DIR / "mslink.ninja_inc")}', key=cls)

    @staticmethod
    def add_rules(globals_):  # pylint: disable=missing-docstring
        LinkFlag = globals_['LinkFlag'] = lambda *args, **kwargs: Flag('LINKFLAGS', *args, **kwargs)
        Lib = globals_['Lib'] = lambda lib: LinkFlag((f'"{lib}.lib"',))  # pylint:disable=invalid-name
        LibDir = globals_['LibDir'] = lambda path: LinkFlag(('/LIBPATH:', Path(path)))  # pylint:disable=invalid-name
        LinkerTarget = globals_['LinkerTarget'] = lambda *args, libs=(), implicit_inputs=(), **kwargs: Target(*args, implicit_inputs=chain(libs, implicit_inputs), **kwargs)

        globals_['Archive'] = lambda lib, objects, *, whole=False, pic=False, exported_flags=(), **kwargs: LinkerTarget(objects, (E.build_path / (lib + '.lib'),), 'lib', exported_flags=chain(exported_flags, (LibDir(E.build_path), Lib(lib))), **kwargs)  # noqa: E501
        globals_['SharedObject'] = lambda lib, objects, *, implicit_outputs=(), flags=(), exported_flags=(), **kwargs: LinkerTarget(objects, (E.build_path / (lib + '.lib'),), 'dll', implicit_outputs=chain(implicit_outputs, (E.build_path / (lib + '.dll'),)), flags=chain(flags, (Variable('DLL', (E.build_path / lib).with_suffix('.dll')),)), exported_flags=chain(exported_flags, (LibDir(E.build_path), Lib(lib))), **kwargs)  # noqa: E501
        globals_['Executable'] = lambda executable, objects, *, pic=False, **kwargs: LinkerTarget(objects, ((E.build_path / executable).with_suffix('.exe'),), 'exe', **kwargs)


class Toolset:
    @classmethod
    def apply_to_env(cls):  # pylint: disable=missing-docstring
        CompilerToolset.apply_to_env()
        LinkerToolset.apply_to_env()

    @staticmethod
    def add_rules(globals_):  # pylint: disable=missing-docstring
        CompilerToolset.add_rules(globals_)
        LinkerToolset.add_rules(globals_)
