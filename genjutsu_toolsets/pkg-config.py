'''
pkg-config genjutsu toolset
'''
from subprocess import check_output
from genjutsu_toolsets.c import CFlag, LinkFlag

def PkgConfigFlags(name):
   return (CFlag(check_output(f'pkg-config --cflags {name}')), LinkFlag(check_output(f'pkg-config --libs {name}')))
