'''
Clang genjutsu toolset
'''
from genjutsu_toolsets.gnu_toolset import *

register_initializer('clang', ('debug', 'low_overhead', 'profile', 'profile_instr_record', 'profile_instr_use', 'profile_sample_record', 'profile_sample_use',
                               'coverage', 'sanitize_address', 'sanitize_thread', 'sanitize_memory', 'sanitize_undefined'))
