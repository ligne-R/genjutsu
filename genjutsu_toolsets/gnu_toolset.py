'''
Rules for toolsets following the CLI of GCC (namely GCC and Clang)
'''
from functools import partial
from itertools import chain
from pathlib import Path
from platform import system as system_
from typing import Collection, Optional

from genjutsu import Alias, Apply, E, Env, Flag, Inject, PathLike, Target, TargetType, VariableType, escape_path, get_resource_file, TargetOrPath, resolve_as_input


def register_initializer(compiler_name: str, flavours: Collection[str]=()) -> None:
    def apply_to_env():  # pylint: disable=missing-docstring
        system_name, *_ = system_().lower().split('-')
        full_name = f'{compiler_name}-{system_name}'
        Inject(lambda env, flavour: '\n'.join((f'include {escape_path(get_resource_file(full_name + ".ninja_inc"))}',
                                               f'include {escape_path(get_resource_file("gnu_toolset.ninja_inc"))}')), key=__file__)
        for flavour in set(flavours) | {'default'}:
            Apply((Flag(flags, (f'${flags}_{flavour.upper()}',)) for flags in ('CPPFLAGS', 'CFLAGS', 'CXXFLAGS', 'ARFLAGS', 'LDFLAGS',)), flavour=flavour)
    Env.register_initializer(apply_to_env)


def IncludeDir(path: PathLike, *, system: bool=False) -> VariableType:  # pylint: disable=invalid-name,missing-docstring
    return Flag('CPPFLAGS', ('-isystem' if system else '-I', Path(path)))


def CxxDef(key: str, value=None) -> VariableType:  # pylint: disable=invalid-name,missing-docstring
    return Flag('CPPFLAGS', ('-D', key, '=', value) if value is not None else ('-D', key))


CFlag = partial(Flag, 'CFLAGS')


def Pch(pch: TargetOrPath, *, name: Optional[str]=None, **kwargs) -> TargetType:  # pylint: disable=invalid-name,missing-docstring
    pch_resolved = resolve_as_input(pch)
    output = (E.build_path / pch_resolved.parent / (name or pch_resolved.name)).with_suffix('.pch')
    target = Target((pch_resolved,), (output,), 'pch', **kwargs)
    Alias('pch', (target,))
    return target


def Cxx(cxx: TargetOrPath, *, name: Optional[str]=None, pch: Optional[TargetOrPath]=None, pic:bool=False, implicit_inputs=(), flags=(), preprocess_only=False, compile_only=False, quick_build_alias='{source}:{flavour}', **kwargs) -> TargetType:  # pylint: disable=invalid-name,missing-docstring
    if pch:
        flags = chain(flags, (CFlag(('-include ', pch.inputs[0])),))
        implicit_inputs = chain(implicit_inputs, pch.outputs)
    if pic:
        flags = chain(flags, (CFlag(('$CFLAGS_PIC',)),))
    if preprocess_only:
        flags = chain(flags, (CFlag(('$CFLAGS_PREPROCESS_ONLY',)),))
    if compile_only:
        flags = chain(flags, (CFlag(('$CFLAGS_COMPILE_ONLY', '$AS_FLAGS')),))
    suffix = '.i' if preprocess_only else '.s' if compile_only else '.o'
    cxx_resolved = resolve_as_input(cxx)
    output = (E.build_path / cxx_resolved.parent / ('pic' if pic else '') / (name or cxx_resolved.name)).with_suffix(suffix)
    target = Target((cxx_resolved,), (output,), 'cxx', implicit_inputs=implicit_inputs, flags=flags, **kwargs)
    if quick_build_alias is not None:
        Alias(quick_build_alias.format(source=E.base_dir / cxx_resolved, flavour='{flavour}'), (target,))
    return target


LinkFlag = partial(Flag, 'LDFLAGS')


def LibFlag(value, *, static=False) -> VariableType:  # pylint: disable=invalid-name,missing-docstring
    return Flag('LDLIBS' if not static else 'LDLIBS_STATIC', value)


def LibDir(path) -> VariableType:  # pylint: disable=invalid-name,missing-docstring
    return LinkFlag(('-L', Path(path)))


def Lib(lib, *, static=False) -> VariableType:  # pylint: disable=invalid-name,missing-docstring
    return LibFlag(('-l', lib), static=static)


def _LinkerTarget(*args, libs=(), implicit_inputs=(), **kwargs) -> TargetType:  # pylint: disable=invalid-name,missing-docstring
    return Target(*args, implicit_inputs=chain(implicit_inputs, libs), **kwargs)


def Archive(archive, objects, *, whole=False, pic=False, exported_flags=(), **kwargs) -> TargetType:  # pylint: disable=invalid-name,missing-docstring
    Lib_, LibFlag_ = partial(Lib, static=not pic), partial(LibFlag, static=not pic)  # pylint: disable=invalid-name
    if whole:
        exported_flags = chain((LibDir(E.build_path), LibFlag_(('-Wl,--whole-archive',)), Lib_(archive), LibFlag_(('-Wl,--no-whole-archive',))), exported_flags)
    else:
        exported_flags = chain((LibDir(E.build_path), Lib_(archive)), exported_flags)
    return _LinkerTarget(objects, (E.build_path / ('lib' + archive + '.a'),), 'ar', exported_flags=exported_flags, **kwargs)


def SharedObject(so, objects, *, exported_flags=(), **kwargs) -> TargetType:  # pylint: disable=invalid-name,missing-docstring
    return _LinkerTarget(objects, (E.build_path / ('lib' + so + '.so'),), 'so', exported_flags=chain((LibDir(E.build_path), Lib(so)), exported_flags), **kwargs)


def Executable(executable, objects, *, pic=False, flags=(), **kwargs) -> TargetType:  # pylint: disable=invalid-name,missing-docstring
    if pic:
        flags = chain(flags, (Flag('LDFLAGS', '$LDFLAGS_PIC'),))
    return _LinkerTarget(objects, (E.build_path / executable,), 'exe', flags=flags, **kwargs)
