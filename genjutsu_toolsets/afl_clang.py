'''
AFL Clang genjutsu toolset
'''
from genjutsu.genjutsu import TargetType
from genjutsu_toolsets.gnu_toolset import *

def Pch(pch: TargetType, *, name:str =None, **kwargs):  # pylint: disable=invalid-name,missing-docstring
    return None

register_initializer('afl-clang')
