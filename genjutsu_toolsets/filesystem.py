'''
Filesystem genjutsu toolset
'''
from itertools import chain
from pathlib import Path, PurePath
from platform import system

from genjutsu import E, Env, Inject, Target, escape_path, get_resource_file
from genjutsu.genjutsu import PathLike, TargetOrPath, TargetType


def Directory(name: PathLike, **kwargs):  # pylint: disable=invalid-name
    return Target((), (E.build_path / name,), 'mkdir', **kwargs)


def Copy(from_: TargetOrPath, to: PathLike=None, *, implicit_inputs=(), **kwargs):  # pylint: disable=invalid-name
    resolved_from: PurePath = from_.outputs[0] if isinstance(from_, TargetType) else from_
    if to is None:
        to = resolved_from 
    elif isinstance(to, str):
        to = to.format(path=resolved_from.parent, name=resolved_from.name)
    return Target((E.source_path / resolved_from,), (E.build_path / to,), 'copy', implicit_inputs=chain((Directory(Path(to).parent),), implicit_inputs), **kwargs)


def _initializer():
    system_name, *_ = system().lower().split('-')
    filename:str = f'filesystem-{system_name}.ninja_inc'
    Inject(lambda env, flavour: f'include {escape_path(get_resource_file(filename))}', key=__file__)

Env.register_initializer(_initializer)
