'''
GCC genjutsu toolset
'''
from genjutsu_toolsets.gnu_toolset import *

register_initializer('gcc', ('debug', 'low_overhead'))
