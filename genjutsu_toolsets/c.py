import os

if os.environ.get("GENJUTSU_C_TOOLSET") == "afl":
    from genjutsu_toolsets.afl_clang import *
elif os.environ.get("GENJUTSU_C_TOOLSET") == "clang":
    from genjutsu_toolsets.clang import *
else:
    from genjutsu_toolsets.gcc import *
